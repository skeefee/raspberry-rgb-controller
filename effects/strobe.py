#!/usr/bin/python

import os
import sys
import termios
import tty
import pigpio
import time
from thread import start_new_thread

speed = float(sys.argv[1])
bright = float(sys.argv[2])

r = float(sys.argv[3])
g = float(sys.argv[4])
b = float(sys.argv[5])

RED_PIN   = float(sys.argv[6])
GREEN_PIN = float(sys.argv[7])
BLUE_PIN  = float(sys.argv[8])

abort = False

pi = pigpio.pi()

def setLights(pin, brightness):
	realBrightness = int(int(brightness) * (float(bright) / 255.0))
	pi.set_PWM_dutycycle(pin, realBrightness)
	
setLights(RED_PIN, 0)
setLights(GREEN_PIN, 0)
setLights(BLUE_PIN, 0)

while abort == False:
	
	setLights(RED_PIN, r/10)
	setLights(BLUE_PIN, b/10)
	setLights(GREEN_PIN, g/10)
	
	time.sleep(speed)
	
	setLights(RED_PIN, r)
	setLights(BLUE_PIN, b)
	setLights(GREEN_PIN, g)
	
	time.sleep(speed)