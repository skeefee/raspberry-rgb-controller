var debounce = false;

function updateForm(){
	clearTimeout( debounce );
	debounce = setTimeout(function(){
		var data = $("form").serialize();

		$.ajax({
			url: "task.php",
			data: "query=update&"+data,
			success: function(){

			}
		});
		debounce = false;
	}, 100);
};


function updateValues(){
	
	if( !debounce ){
		$.ajax({
			url: "/json/main.json",
			dataType: "JSON",
			cache: false,
			success:function(response){
				$("input").each(function(){
					var obj = $(this);
					var i = obj.attr("name");

					if( obj.attr("type") == "hidden" ){
						obj.val( response[i] ).trigger("update");	
					}
					else if( obj.attr("type") == "checkbox" ){
						if( response[i] == true ){
							obj.prop("checked", true);
						}else{
							obj.prop("checked", false);
						};
					}
					else if( obj.attr("type") == "radio" ){
						if( response[i] == obj.val() ){
							obj.prop("checked", true);
						}else{
							obj.prop("checked", false);
						};	
					}
				});
				setTimeout(function(){
					updateValues();
				}, 1000);
			}
		});
	}else{
		setTimeout(function(){
			updateValues();
		}, 1000);
	}
};