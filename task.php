<?php
	
	require("helpers.php");

	$data = get("pins");

	$red_pin = $data->{"red_pin"};
	$green_pin = $data->{"green_pin"};
	$blue_pin = $data->{"blue_pin"};

	function killPythonScripts(){
		shell_exec('pkill -f "python /var/www/effects/fading.py"');
		shell_exec('pkill -f "python /var/www/effects/strobe.py"');
	};

	if( $_GET["query"] == "update" ){
		
		killPythonScripts();
		
		$colors = split("-", $_GET["color"]);
		
		if( !$_GET["status"] ){
			
			shell_exec('pigs p 23 0');
			shell_exec('pigs p 24 0');
			shell_exec('pigs p 25 0');
			
		}else{
			
			if( $_GET["effect"] == "fade-colors" ){
				$speed = $_GET["speed"]/1000;
				$brightness = $_GET["brightness"]*2.55;
				
				shell_exec('python /var/www/effects/fading.py "'.$speed.'" "'.$brightness.'" "'.$red_pin.'" "'.$green_pin.'" "'.$blue_pin.'"');
			}
			elseif( $_GET["effect"] == "strobe" ){
				$speed = (100 - $_GET["speed"]) /100;
				$brightness = $_GET["brightness"]*2.55;
				$red = $colors[0];
				$green = $colors[1];
				$blue = $colors[2];
				
				shell_exec('python /var/www/effects/strobe.py "'.$speed.'" "'.$brightness.'" "'.$red.'" "'.$green.'" "'.$blue.'" "'.$red_pin.'" "'.$green_pin.'" "'.$blue_pin.'"');	
				
			}
			else{

				$red = $colors[0];
				$green = $colors[1];
				$blue = $colors[2];

				$brightness = $_GET["brightness"] / 2;
				$step = 255 / 50;
				$brightness = 50 - $brightness;


				$blue = $blue - ($brightness * ($blue / 50));
				$green = $green - ($brightness * ($green / 50));
				$red = $red - ($brightness * ($red / 50));
				
				shell_exec('pigs p '.$green_pin.' '.$green);
				shell_exec('pigs p '.$red_pin.' '.$red);
				shell_exec('pigs p '.$blue_pin.' '.$blue);
			}
		}
		
	}
	elseif( $_GET["query"] == "save" ){
		$data = get("main");

		foreach ($data as $key => $value){
			if( isset($_GET[$key]) ){
				$newValue = $_GET[$key];
				
				if( $newValue == "on" ){ $newValue = true; }
				
				$data->{$key} = $newValue;
			}else{
				$data->{$key} = false;
			}
			
		}
		
		if( $data ){
			set("main", $data);
		};
		
	}

?>