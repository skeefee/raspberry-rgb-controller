<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Wiseman Interactive Oü  www.wiseman.ee"/>
<link type="text/css" rel="stylesheet" href="static/styles/default.css" />
<link rel="shortcut icon"  href="static/imgs/favicon.png" type="image/x-icon"/>
<link rel="apple-touch-icon" href="static/imgs/favicon.png" />
<script type="text/javascript" language="javascript" src="static/js/lib/jquery-1.10.2.min.js" ></script>
<script type="text/javascript" language="javascript" src="static/js/lib/device.min.js" ></script>
<script type="text/javascript" language="javascript" data-main="static/js/main" src="static/js/require.js"></script>
<title> RGB Controllers </title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="<?php
	if( isset($_COOKIE["viewport"]) ){
		echo $_COOKIE["viewport"];	
	}
?>" />

</head>
<body>

<div class="loader"></div>
	
</body>
</html>