function getLanguage(){
	
	$.ajax({
		url: "json/languages/english.json",
		dataType: "JSON",
		success: function( response ){
			getTemplate( response  );
		}
	});
	
};

function getTemplate( lang ){
	
	var template;
	
	if( device.ios() || device.desktop() ){
		template = "ios_8";
		
		if( device.tablet() || device.desktop() ){
			var viewport = "width=1200px, user-scalable=no";
			
			$("meta[name='viewport']").attr("content", viewport);
			$("body").addClass("show-colorwheel");
			createCookie("viewport", viewport, 365); 
		}else{
			var viewport = "width=375px, user-scalable=no";
			$("meta[name='viewport']").attr("content", viewport);
			createCookie("viewport", viewport, 365);
		}
	}
	
	var url = "static/themes/"+template+"/"+template;
	
	$("head").append('<link type="text/css" rel="stylesheet" href="'+url+'.css?urlArgs='+urlArgs+'" />');
	
	$.ajax({
		url: url+".html",
		cache: cache,
		success: function( response ){
			var swig = require('swig');

			swig.setDefaults({ cache: false, autoescape: false });

			var myhtml = swig.render( response, {locals:{
				lang: lang
			}});
			
			$("body").append( myhtml );
			
			setTimeout(function(){
				getJavascript( url );
			}, 100);
		}
	});
	
};

function getJavascript( url ){
	require([url+".js"], function(){
		
		$(window).bind("focus", function(){
			updateValues();
		});
		
		updateValues();
		
		setTimeout(function(){
			$(".loader").fadeOut(500);
		},500);
	});
};

function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

document.createElement('header');
document.createElement('nav');
document.createElement('section');
document.createElement('article');
document.createElement('aside');
document.createElement('footer');
document.createElement('figure');