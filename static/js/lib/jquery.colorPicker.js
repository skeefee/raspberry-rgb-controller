$.fn.colorPicker = function(){
	var main = $(this);
	
	function findPos(obj) {
		 var curleft = 0, curtop = 0;
		 if (obj.offsetParent) {
			  do {
					curleft += obj.offsetLeft;
					curtop += obj.offsetTop;
			  } while (obj = obj.offsetParent);
			  return { x: curleft, y: curtop };
		 }
		 return undefined;
	}

	
	var input = main.find("input");

	input.bind("update", function(){
		var inputRGB = input.val().split("-");
		var inputHEX = rgbToHex(inputRGB[0], inputRGB[1], inputRGB[2]);

		main.css({
			backgroundColor: "#"+inputHEX
		});

	}).trigger("update");

	var canvas = main.find("canvas");

	var c = canvas[0];

	var currentColor = '';
	var moveEvent = events.mousemove;
	if( moveEvent == "mousemove" ){
		moveEvent = "click";
	};
	canvas.bind(moveEvent, function(e) {

		e.preventDefault();
		var pos = findPos(this);
		if( isMobile ){
			var x = e.originalEvent.touches[0].pageX - pos.x;
			var y = e.originalEvent.touches[0].pageY - pos.y;
			var movePos = e.originalEvent.touches[0].pageX;
			
		}else{
			var x = e.pageX - pos.x;
			var y = e.pageY - pos.y;
		};

		var coord = "x=" + x + ", y=" + y;
		var c = this.getContext('2d');
		var p = c.getImageData(x, y, 1, 1).data; 
		var hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);

		if( hex == "#000000" ){ return false; }

		main.css({
			backgroundColor: hex
		});

		if( currentColor == hex ){ return false; }
		currentColor = hex;

		input.val( hexToRgb(hex).r+"-"+hexToRgb(hex).g+"-"+hexToRgb(hex).b );

		presets.filter(":checked").prop("checked", false);

		updateForm();

	});

	$(window).bind("resize", function(){

		canvas.attr({
			width: main.width(),
			height: main.width()
		}).css("top", (main.height() - main.width()) /2);

		var ctx = c.getContext("2d");
		var img = new Image();
		img.onload = function(){

			ctx.drawImage(img,0,0,c.width,c.height);
		}

		img.src = "static/imgs/favicon.png";

	}).trigger("resize");

};
