$.fn.slider = function(){
	$(this).each(function(){
		var main = $(this);
		var knob = main.find(".knob");
		var input = main.find("input");
		var highlight = main.find(".highlight");
		var value = input.val();

		knob.bind(events.mousedown, function(e){
			e.preventDefault();
			var maxWidth = main.find(".bar").width();

			if( isMobile ){
				var startPos = e.originalEvent.touches[0].pageX;
			}else{
				var startPos = e.pageX;
			};

			var knobWidth = knob.width();
			var mainLeft = main.offset().left;
			var origPos = knob.position().left;

			$(document).bind(events.mousemove, function(e){
				e.preventDefault();
				
				if( isMobile ){
					var movePos = e.originalEvent.touches[0].pageX;;
				}else{
					var movePos = e.pageX;
				};

				var currentPos = movePos - startPos + origPos;

				if( currentPos < 0 ){ currentPos = 0; }

				if( currentPos >= maxWidth ){ currentPos = maxWidth; }

				var percentage = (currentPos / maxWidth ) *100;

				setPosition( percentage );

			});

			$(document).bind(events.mouseup, function(){
				updateForm();
				$(document).unbind(events.mousemove+" "+events.mouseup);
			});
		});

		input.bind("update", function(){
			setPosition( input.val(), true);	
		});

		function setPosition( percentage, flag){

			highlight.css({
				width: percentage+"%"
			});

			knob.css({
				left: percentage+"%"
			});
			

			input.val( Math.ceil(percentage) );

		};

		setPosition( value, true);
	});
	
};
