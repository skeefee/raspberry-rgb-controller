var isMobile = device.mobile() || device.tablet() ? true : false;
var events = {
	mousedown: isMobile ? "touchstart" : "mousedown",
	mousemove: isMobile ? "touchmove" : "mousemove",
	mouseup: isMobile ? "touchend" : "mouseup",
	click: isMobile ? "click" : "click"
};

var cache = false;

if( !cache ){
	var urlArgs = 'bust='+ (new Date()).getTime();
}else{
	var urlArgs = '';
}

require.config({
	urlArgs: urlArgs,
   baseUrl: "static/js/lib",
   paths: {
      "tasks": "tasks",
      "functions": "jquery.functions",
      "colorpicker": "jquery.colorPicker",
      "less": "less.min",
      "slider": "jquery.slider",
		"swig": "swig.min"
   }
});

require(["swig", "tasks", "colorpicker", "slider", "functions"], function(){
	getLanguage();
});
