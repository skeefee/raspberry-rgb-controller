$(".slider").slider();
$(".colorwheel").colorPicker();

var presets = $("input[name='preset']");

var toggles = $("[data-toggleClass]");


var toggleEvent = events.click;

if( device.tablet() || device.desktop() ){
	setTimeout(function(){
		toggles.filter("[data-toggleClass='show-colorwheel']").trigger(toggleEvent);
	}, 200);
	toggleEvent = events.mousedown;
};

toggles.bind(toggleEvent, function(e){
	e.preventDefault();
	$("body").removeAttr("class").addClass( $(this).attr("data-toggleClass")+" pane-visible");
	toggles.removeClass("active");
	$(this).addClass("active");
});

$("header.main .back").bind(events.mousedown, function(e){
	e.preventDefault();
	$("body").removeAttr("class");
});

$("input[type='checkbox'],input[type='radio']").bind(events.mousedown, function(e){
	e.preventDefault();
	var obj = $(this);
	if( obj.is(":checked") ){
		obj.prop("checked", false);
	}else{
		obj.prop("checked", true);
	};

	updateForm();

}).bind(events.click, function(e){
	e.preventDefault();
});