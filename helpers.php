<?php
	
	//load json file and return results in PHP array
	function get($filename){
		$file = file_get_contents("./json/".$filename.".json", true) or die("unable");
		$json = json_decode($file);
		return $json;
	}

	function set($filename, $data){
		$data = json_encode($data);
		$file = fopen("json/".$filename.".json", "w") or die("Unable to open file!");
		
		fwrite($file, $data);
		fclose($file);
	}

?>